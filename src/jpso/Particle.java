package jpso;

/**
 * A class for overriding toString method for each particle.
 * 
 * @author Manolis Georgioudakis
 */

public class Particle
{
	public double bestFitness;
	public double[] bestPosition; // best position found so far by this Particle
	public double fitness;
	public double[] position; // equivalent to x-Values and/or solution
	public double[] velocity;

	public Particle(final double[] position, final double fitness, 
					final double[] velocity, final double[] bestPosition,
					final double bestFitness)
	{
		this.position = new double[position.length];
		System.arraycopy(position, 0, this.position, 0, position.length);
		this.fitness = fitness;
		this.velocity = new double[velocity.length];
		System.arraycopy(velocity, 0, this.velocity, 0, velocity.length);
		this.bestPosition = new double[bestPosition.length];
		System.arraycopy(bestPosition, 0, this.bestPosition, 0, bestPosition.length);
		this.bestFitness = bestFitness;
	}

	@Override
	public String toString()
	{
		String s = "";
		s += "==========================\n";
		s += "Dimension                x[0]          x[1]\n";
		s += "Position =      ";
		for (final double element : this.position)
		{
			s += String.format("%15.8f", element);
		}
		s += "\n";
		s += "Velocity =      ";
		for (final double element : this.velocity)
		{
			s += String.format("%15.8f", element);
		}
		s += "\nFitness =       " + String.format("%15.8f", this.fitness);
		s += "\n";
		s += "Best Position = ";
		for (final double element : this.bestPosition)
		{
			s += String.format("%15.8f", element);
		}
		s += "\n";
		s += "Best Fitness =  " + String.format("%15.8f", this.bestFitness) + "\n";
		return s;
	}

} // class Particle
