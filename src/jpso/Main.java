package jpso;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * A Particle Swarm Optimization class to solve optimization problems.
 * 
 * See more: http://msdn.microsoft.com/en-us/magazine/hh335067.aspx
 *
 * @author Manolis Georgioudakis
 */

public class Main

{
	private static java.util.Random ran = null;

	public static void main(final String[] args) throws IOException
	{
		try
		{

			String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
			File file = new File("JPSO.log");
			BufferedWriter output = new BufferedWriter(new FileWriter(file));

			
			output.write("JPSO Summary\n" + "--------------\n");
	        output.write("Program started at: " + timeStamp + "\n");
			output.write("\nObjective function to minimize has dimension = 2\n");
	        output.write("Objective function is f(x) = 3 + (x0^2 + x1^2)\n");

			ran = new java.util.Random(0);
			
			final int numberParticles = 10;
			final int numberIterations = 1000;
			int iteration = 0;
			final double tolerance = 10e-10;
			final int dimension = 2; // dimensions
			final double minX = -2.0;
			final double maxX = 2.0;

			output.write("Range for all x values is " + minX + " <= x <= " + maxX + "\n");
			output.write("Number particles in swarm = " + numberParticles + "\n");

			final Particle[] swarm = new Particle[numberParticles];
			final double[] bestGlobalPosition = new double[dimension]; // best solution found by any particle in the swarm.
			double bestGlobalFitness = Double.MAX_VALUE; // smaller values better - implicit initialization to all 0.0

			final double minV = -1.0 * maxX;
			final double maxV = maxX;
			
			System.out.println("\nInitializing swarm with random positions/solutions...");
			for (int i = 0; i < swarm.length; ++i) // initialize each Particle in the swarm
			{
				final double[] randomPosition = new double[dimension];
				for (int j = 0; j < randomPosition.length; ++j)
				{
					final double lo = minX;
					final double hi = maxX;
					randomPosition[j] = ((hi - lo) * ran.nextDouble()) + lo;
				}
				// double fitness = SphereFunction(randomPosition); // smaller values are better
				// double fitness = GP(randomPosition); // smaller values are better
				final double fitness = ObjectiveFunction(randomPosition);
				final double[] randomVelocity = new double[dimension];

				for (int j = 0; j < randomVelocity.length; ++j)
				{
					final double lo = -1.0 * Math.abs(maxX - minX);
					final double hi = Math.abs(maxX - minX);
					randomVelocity[j] = ((hi - lo) * ran.nextDouble()) + lo;
				}
				swarm[i] = new Particle(randomPosition, fitness, randomVelocity, randomPosition, fitness);

				// does current Particle have global best position/solution?
				if (swarm[i].fitness < bestGlobalFitness)
				{
					bestGlobalFitness = swarm[i].fitness;
					System.arraycopy(swarm[i].position, 0, bestGlobalPosition, 0, swarm[i].position.length);
				}
			} // initialization

			System.out.println("\nInitialization complete\n");
			output.write("\nInitial Data:\n");
			output.write("Initial best fitness = " + bestGlobalFitness +"\n");
			output.write("Initial best position/solution:"+"\n");
			for (int i = 0; i < bestGlobalPosition.length; ++i)
			{
				output.write("x" + i + " = " + String.format("%15.8f", bestGlobalPosition[i]) + "\n");
			}

			final double w = 0.729; // inertia weight. see http://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=00870279
			final double c1 = 1.49445; // cognitive/local weight
			final double c2 = 1.49445; // social/global weight
			double r1, r2; // cognitive and social randomizations

			output.write("\nEntering main PSO processing loop\n");
			double start_time = System.currentTimeMillis();

			while (iteration < numberIterations)
			{

				++iteration;
				final double[] newVelocity = new double[dimension];
				final double[] newPosition = new double[dimension];
				double newFitness;

				for (final Particle element : swarm)
				{
					final Particle currP = element;

					for (int j = 0; j < currP.velocity.length; ++j) // each x value of the velocity
					{
						r1 = ran.nextDouble();
						r2 = ran.nextDouble();

						newVelocity[j] =
								(w * currP.velocity[j])
										+ (c1 * r1 * (currP.bestPosition[j] - currP.position[j]))
										+ (c2 * r2 * (bestGlobalPosition[j] - currP.position[j]));

						if (newVelocity[j] < minV)
						{
							newVelocity[j] = minV;
						}
						else
							if (newVelocity[j] > maxV)
							{
								newVelocity[j] = maxV;
							}
					}

					System.arraycopy(newVelocity, 0, currP.velocity, 0, newVelocity.length);

					for (int j = 0; j < currP.position.length; ++j)
					{
						newPosition[j] = currP.position[j] + newVelocity[j];
						if (newPosition[j] < minX)
						{
							newPosition[j] = minX;
						}
						else
							if (newPosition[j] > maxX)
							{
								newPosition[j] = maxX;
							}
					}

					System.arraycopy(newPosition, 0, currP.position, 0, newPosition.length);
					newFitness = ObjectiveFunction(newPosition);
					currP.fitness = newFitness;

					// check norm
					double norm = (newFitness-bestGlobalFitness)/bestGlobalFitness;
					if (Math.abs(norm)> tolerance)
					{
						output.write(String.format("%15.8f", Math.abs(norm)) + "\n");
						//System.exit(0);
					}

					if (newFitness < currP.bestFitness)
					{
						System.arraycopy(newPosition, 0, currP.bestPosition, 0, newPosition.length);
						currP.bestFitness = newFitness;
					} 

					if (newFitness < bestGlobalFitness)
					{
						System.arraycopy(newPosition, 0, bestGlobalPosition, 0, newPosition.length);
						bestGlobalFitness = newFitness;
					} 

				} // each Particle

				output.write("\nIteration: " + iteration +"\n");
				for (int i = 0; i < swarm.length; ++i)
				{
					output.write(swarm[i].toString());
				}

			} // while

			output.write("\nProcessing complete\n");
			output.write("Final best fitness = ");
			output.write(String.format("%10.4f", bestGlobalFitness));
			output.write("\nBest position/solution:\n");
			System.out.println("\nBest position/solution:\n");
			for (int i = 0; i < bestGlobalPosition.length; ++i)
			{
				System.out.println("x" + i + " = " + String.format("%.10f", bestGlobalPosition[i]));
				output.write("x" + i + " = " + String.format("%.10f", bestGlobalPosition[i]) + "\n");
			}
			
			double end_time = System.currentTimeMillis();
			double elapsed_time = end_time - start_time;
			output.write("\nExecution Time: " + elapsed_time + " miliseconds" + "\n");			
			output.write("\nEnd JPSO logging");

			output.close();

		}
		catch (final RuntimeException ex)
		{
			System.out.println("Fatal error: " + ex.getMessage());
		}
	} // End of main Particle Swarm Optimization procedure

	private static double ObjectiveFunction(final double[] x)
	{
		return 3.0 + (x[0] * x[0]) + (x[1] * x[1]); // f(x, y) = 3 + x^2 + y^2
		// return ((((1 - x[0]) * (1 - x[0])) + (100 * (x[1] - (x[0] * x[0])) * (x[1] - (x[0] * x[0]))))); // Rosenbrock:
																										// f(x, y) =
																										// (1-x)^2 + 100
																										// *
		// (y-x^2)^2
	}
} // End Class Program