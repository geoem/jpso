Description
===========

This project contains two classes for solving optimization problems using 
the `Particle Swarm Optimization` algorithm.

**IMPORTANT NOTE**

The code is based on a C# code published in MSDN Magazine. See more at:
http://msdn.microsoft.com/en-us/magazine/hh335067.aspx

### Testing functions

You may use other test funtions by changing the following part of the code:

	private static double ObjectiveFunction(final double[] x)
	{
		return 3.0 + (x[0] * x[0]) + (x[1] * x[1]); // f(x, y) = 3 + x^2 + y^2
	}

Requirements
============

Java JDK/JRE
Eclipse IDE

## Platform

* Linux
* MacOS
* Windows

License and Author
==================

* Author: Manolis Georgioudakis (<geoem@mail.ntua.gr>)

Copyright: 2013

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
